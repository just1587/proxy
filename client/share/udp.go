package chshare

import (
	"encoding/gob"
	"io"
)

type UdpPacket struct {
	Src     string
	Dst     string
	Payload []byte
}

//udpChannel encodes/decodes udp payloads over a stream
type UdpChannel struct {
	R *gob.Decoder
	W *gob.Encoder
	C io.Closer
}

func (o *UdpChannel) Encode(src string, dst string, b []byte) error {
	return o.W.Encode(UdpPacket{
		Src:     src,
		Dst:     dst,
		Payload: b,
	})
}

func (o *UdpChannel) Decode(p *UdpPacket) error {
	return o.R.Decode(p)
}
