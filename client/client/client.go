package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/websocket"
	"github.com/jpillora/backoff"
	chshare "github.com/jpillora/chisel/share"
	"golang.org/x/crypto/ssh"
	"golang.org/x/sync/errgroup"
)

//Config represents a client configuration
type Config struct {
	shared              *chshare.Config
	Fingerprint         string
	CAFile              string
	KeepAlive           time.Duration
	MaxRetryCount       int
	MaxRetryInterval    time.Duration
	ServerList          []string
	SkipTlsVerification bool
	Remotes             []string
	HostHeader          string
	Debug               bool
}

//Client represents a client instance
type Client struct {
	*chshare.Logger
	config    *Config
	sshConfig *ssh.ClientConfig
	tlsConfig *tls.Config
	servers   []string
	stop      func()
	eg        *errgroup.Group
	connStats chshare.ConnStats
	tunnel    *Tunnel
}

//NewClient creates a new client instance
func NewClient(config *Config) (*Client, error) {
	var err error
	config.ServerList, err = formatServers(config.ServerList)
	if err != nil {
		return nil, fmt.Errorf("formatServers error: %w", err)
	}
	if config.MaxRetryInterval < time.Second {
		config.MaxRetryInterval = 5 * time.Minute
	}
	shared := &chshare.Config{}
	for _, s := range config.Remotes {
		r, err := chshare.DecodeRemote(s)
		if err != nil {
			return nil, fmt.Errorf("Failed to decode remote '%s': %s", s, err)
		}
		shared.Remotes = append(shared.Remotes, r)
	}
	config.shared = shared
	client := &Client{
		Logger:  chshare.NewLogger("client"),
		config:  config,
		servers: config.ServerList,
	}
	client.Info = true
	client.Debug = config.Debug
	client.Debugf("tunnel is restarting...")

	client.sshConfig = &ssh.ClientConfig{
		Auth:            []ssh.AuthMethod{ssh.Password("")},
		ClientVersion:   "SSH-" + chshare.ProtocolVersion + "-client",
		HostKeyCallback: client.verifyServer,
		Timeout:         30 * time.Second,
	}

	if config.CAFile != "" {
		data, err := ioutil.ReadFile(config.CAFile)
		if err != nil {
			return nil, fmt.Errorf("could not read certificate authority: %s", err)
		}

		certPool := x509.NewCertPool()
		if !certPool.AppendCertsFromPEM(data) {
			return nil, fmt.Errorf("could not append certificate data")
		}

		client.tlsConfig = &tls.Config{RootCAs: certPool}
	}
	if config.SkipTlsVerification {
		if client.tlsConfig != nil {
			client.tlsConfig.InsecureSkipVerify = true
		} else {
			client.tlsConfig = &tls.Config{InsecureSkipVerify: true}
		}
	}

	client.tunnel = NewTunnel(tunnelConfig{
		Logger:    client.Logger,
		keepAlive: client.config.KeepAlive,
	})
	if client.tlsConfig == nil {
		client.tunnel.tlsConfig = &tls.Config{}
	} else {
		client.tunnel.tlsConfig = client.tlsConfig.Clone()
	}

	return client, nil
}

//Run starts client and blocks while connected
func (c *Client) Run() error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	if err := c.Start(ctx); err != nil {
		return err
	}
	return c.Wait()
}

func (c *Client) verifyServer(hostname string, remote net.Addr, key ssh.PublicKey) error {
	expect := c.config.Fingerprint
	got := chshare.FingerprintKey(key)
	if expect != "" && !strings.HasPrefix(got, expect) {
		return fmt.Errorf("Invalid fingerprint (%s)", got)
	}
	//overwrite with complete fingerprint
	c.Infof("Fingerprint %s", got)
	return nil
}

//Start client and does not block
func (c *Client) Start(ctx context.Context) error {
	ctx, cancel := context.WithCancel(ctx)
	c.stop = cancel
	eg, ctx := errgroup.WithContext(ctx)
	c.eg = eg

	//connect to chisel server
	eg.Go(func() error {
		return c.connectionLoop(ctx)
	})

	//listen socks
	eg.Go(func() error {
		return c.tunnel.BindRemotes(ctx, c.config.shared.Remotes)
	})

	//stats server
	eg.Go(func() error {
		ss := &statsServer{tunnel: c.tunnel, Logger: c.Logger}
		return ss.Run(ctx, cfg.StatsServerPort)
	})

	return nil
}

func (c *Client) connectionLoop(ctx context.Context) error {
	//connection loop!
	b := &backoff.Backoff{Max: c.config.MaxRetryInterval}
	for {
		c.tunnel.CountStats(StatsTunnelReconnect, 1)
		connected, retry, err := c.connectionOnce(ctx)
		//reset backoff after successful connections
		if connected {
			b.Reset()
		}
		//connection error
		attempt := int(b.Attempt())
		maxAttempt := c.config.MaxRetryCount
		//show error message and attempt counts (excluding disconnects)
		if err != nil && err != io.EOF && !strings.HasSuffix(err.Error(), "use of closed network connection") {
			msg := fmt.Sprintf("[TUNNEL] Connection error: %s", err)
			if attempt > 0 {
				msg += fmt.Sprintf(" (Attempt: %d", attempt)
				if maxAttempt > 0 {
					msg += fmt.Sprintf("/%d", maxAttempt)
				}
				msg += ")"
			}
			c.Debugf(msg)
		}
		//give up?
		if !retry || (maxAttempt >= 0 && attempt >= maxAttempt) {
			break
		}

		d := b.Duration()
		c.Infof("[TUNNEL] Retrying in %s...", d)
		select {
		case <-chshare.AfterSignal(d):
			continue //retry now
		case <-ctx.Done():
			c.Infof("[TUNNEL] Cancelled")
			return nil
		}
	}
	c.Close()
	return nil
}

//connectionOnce connects to the server and blocks
func (c *Client) connectionOnce(ctx context.Context) (connected, retry bool, err error) {
	//already closed?
	select {
	case <-ctx.Done():
		return false, false, ctx.Err()
	default:
		//still open
	}
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	d := websocket.Dialer{
		NetDial:          websocketDialer,
		ReadBufferSize:   8192,
		WriteBufferSize:  8192,
		HandshakeTimeout: 45 * time.Second,
		TLSClientConfig:  c.tlsConfig,
		Subprotocols:     []string{chshare.ProtocolVersion},
	}

	wsHeaders := http.Header{}
	if c.config.HostHeader != "" {
		wsHeaders = http.Header{
			"Host": {c.config.HostHeader},
		}
	}

	c.tunnel.activeServer = c.tunnel.chooseServer(c.servers)
	c.Infof("[TUNNEL] Connecting to %s\n", strings.Replace(c.tunnel.activeServer, "ws", "http", 1))
	wsConn, _, err := d.Dial(fmt.Sprintf("%s/service", c.tunnel.activeServer), wsHeaders)
	if err != nil {
		return false, true, err
	}

	conn := chshare.NewWebSocketConn(wsConn)
	// perform SSH handshake on net.Conn
	c.Debugf("[TUNNEL] Handshaking...")
	sshConn, chans, reqs, err := ssh.NewClientConn(conn, "", c.sshConfig)
	if err != nil {
		return false, true, err
	}
	defer sshConn.Close()

	c.config.shared.Version = chshare.BuildVersion
	conf, _ := chshare.EncodeConfig(c.config.shared)
	c.Debugf("[TUNNEL] Sending config")
	t0 := time.Now()
	_, configerr, err := sshConn.SendRequest("config", true, conf)
	if err != nil {
		c.Infof("[TUNNEL] Config verification failed")
		return false, true, err
	}
	if len(configerr) > 0 {
		return false, true, errors.New(string(configerr))
	}
	c.Infof("[TUNNEL] Connect Latency %s", time.Since(t0))
	err = c.tunnel.BindSSH(ctx, sshConn, reqs, chans)
	c.Infof("[TUNNEL] Disconnected")
	connected = time.Since(t0) > 5*time.Second
	return connected, true, err
}

//Wait blocks while the client is running.
//Can only be called once.
func (c *Client) Wait() error {
	err := c.eg.Wait()
	if err != nil {
		c.Errorf("client wait error: %v", err)
	}
	return err
}

//Close manually stops the client
func (c *Client) Close() error {
	if c.stop != nil {
		c.stop()
	}
	return nil
}
