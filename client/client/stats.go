package main

import (
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"net/http/pprof"
	"runtime"
	"sync/atomic"
	"time"

	"context"

	chshare "github.com/jpillora/chisel/share"
	"github.com/jpillora/sizestr"
)

const (
	//stats
	StatsTypeBase = iota
	StatsTunnelReconnect
	StatsTcpConns
	StatsUdpPktsSent
	StatsUdpPktsReceived
	StatsBytesSent
	StatsBytesReceived
	StatsDnsResolveCount

	//errors
	ErrorTcpRemoteProxy
	ErrorTcpLocalProxy
	ErrorUdpRemoteProxy
	ErrorUdpLocalProxy
	StatsTypeMax
)

var statsName = []string{
	"",
	"reconnect_times",
	"tcp_conns",
	"udp_pkts_sent",
	"udp_pkts_received",
	"bytes_sent",
	"bytes_received",
	"dns_resolve_count",
	"error_tcp_remote_proxy",
	"error_tcp_local_proxy",
	"error_udp_remote_proxy",
	"error_udp_local_proxy",
}

type statsServer struct {
	*chshare.Logger
	tunnel *Tunnel
}

type tunnelStats struct {
	TunnelConnected bool   `json:"tunnelConnected"`
	TunnelServer    string `json:"tunnelServer"`
}

type debugStats struct {
	NumGoroutines int              `json:"numGoroutines"`
	MemUsage      string           `json:"memUsage"`
	DebugStats    map[string]int64 `json:"debugStats"`
}

type dnsDomainCfg struct {
	IaBypassDomains  []string `json:"ia_bypass_domains"`
	PaDomains        []string `json:"pa_domains"`
	PaNets           []string `json:"pa_nets"`
	DnsBypassDomains []string `json:"dns_bypass_domains"`
}

func (s *statsServer) statsHandler(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, s.tunnel.getTunnelStatus())
}

func (s *statsServer) debugStatsHandler(w http.ResponseWriter, req *http.Request) {
	var ret debugStats
	memStats := runtime.MemStats{}
	runtime.ReadMemStats(&memStats)
	ret.NumGoroutines = runtime.NumGoroutine()
	ret.MemUsage = sizestr.ToString(int64(memStats.Alloc))

	ret.DebugStats = make(map[string]int64)
	for i := StatsTypeBase + 1; i < StatsTypeMax; i++ {
		ret.DebugStats[statsName[i]] = atomic.LoadInt64(&s.tunnel.statsCount[i])
	}
	retBytes, _ := json.MarshalIndent(&ret, "", "\t")
	fmt.Fprintf(w, string(retBytes))
}

func (s *statsServer) debugConfigHandler(w http.ResponseWriter, req *http.Request) {
	ret, _ := json.MarshalIndent(cfg, "", "\t")
	fmt.Fprintf(w, string(ret))
}

func (s *statsServer) versionHandler(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "OS: %s-%s\nVersion: %s-%s\nBuild Time: %s\n", runtime.GOOS,
		runtime.GOARCH, chshare.BuildVersion, buildGitRevison, buildTimestamp)
}

func (s *statsServer) Run(ctx context.Context, port string) error {
	ln, err := net.Listen("tcp", "127.0.0.1:"+port)
	if err != nil {
		return fmt.Errorf("statsServer listen port %s failed: %v", port, err)
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/stats", s.statsHandler)
	mux.HandleFunc("/version", s.versionHandler)

	mux.HandleFunc("/debug/stats", s.debugStatsHandler)
	mux.HandleFunc("/debug/config", s.debugConfigHandler)
	mux.HandleFunc("/debug/pprof/", pprof.Index)
	mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	mux.HandleFunc("/debug/pprof/trace", pprof.Trace)

	srv := &http.Server{
		Handler:      mux,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	go func() {
		<-ctx.Done()
		err := srv.Close()
		s.Debugf("[STATS] Closed, error: %v", err)
	}()
	err = srv.Serve(ln)
	if err != nil && err != http.ErrServerClosed {
		return fmt.Errorf("statsServer Serve failed: %v", err)
	}
	return nil
}
