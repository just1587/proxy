package main

import (
	"context"
	"encoding/gob"
	"fmt"
	"net"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	chshare "github.com/jpillora/chisel/share"
	"github.com/jpillora/sizestr"
	"golang.org/x/crypto/ssh"
	"golang.org/x/sync/errgroup"
)

type udpListener struct {
	*chshare.Logger
	sshTun        sshTunnel
	remote        *chshare.Remote
	inbound       *net.UDPConn
	outbound      *chshare.UdpChannel
	outboundMut   sync.Mutex
	sent, recv    int64
	localUdpConns *udpConns
}

func init() {
	gob.Register(&chshare.UdpPacket{})
}

func isDone(ctx context.Context) bool {
	select {
	case <-ctx.Done():
		return true
	default:
		return false
	}
}

func listenUDP(l *chshare.Logger, sshTun sshTunnel, remote *chshare.Remote) (*udpListener, error) {
	a, err := net.ResolveUDPAddr("udp", remote.Local())
	if err != nil {
		return nil, l.Errorf("resolve: %s", err)
	}
	conn, err := net.ListenUDP("udp", a)
	if err != nil {
		return nil, l.Errorf("listen: %s", err)
	}
	err = setUdpListeningSocketOption(conn)
	if err != nil {
		return nil, l.Errorf("listen: %s", err)
	}
	//ready
	conns := &udpConns{
		Logger: l,
		m:      map[string]*udpConn{},
	}
	u := &udpListener{
		Logger:        l,
		sshTun:        sshTun,
		remote:        remote,
		inbound:       conn,
		localUdpConns: conns,
	}
	return u, nil
}

func (u *udpListener) run(ctx context.Context) error {
	defer u.inbound.Close()
	//udp dosn't accept connections, udp simply forward packets
	//and therefore only needs to listen
	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		return u.runInBound(ctx)
	})
	eg.Go(func() error {
		return u.runOutBound(ctx)
	})
	if err := eg.Wait(); err != nil {
		u.Debugf("[PROXY] [UDP] listen: %s", err)
		return err
	}
	u.Debugf("[PROXY] [UDP] Close %s, sent %s, received %s",
		u.remote.String(), sizestr.ToString(u.sent), sizestr.ToString(u.recv))
	return nil
}

func (u *udpListener) runInBound(ctx context.Context) error {
	const maxMTU = 9012
	buf := make([]byte, maxMTU)
	oob := make([]byte, maxMTU)
	for !isDone(ctx) {
		//read from inbound udp
		u.inbound.SetReadDeadline(time.Now().Add(time.Second))
		n, oobn, _, addr, err := u.inbound.ReadMsgUDP(buf, oob)
		if err != nil {
			if e, ok := err.(net.Error); ok && !e.Timeout() {
				u.Debugf("[PROXY] [UDP] ReadMsgUDP failed: %v", err)
			}
			continue
		}

		b := buf[:n]
		remoteHost, remotePort, startIdx := getOriginalRemoteAddrUDP(addr, oob[:oobn], b, u.Logger)
		if remoteHost == "::" || remoteHost == "0.0.0.0" || remotePort == "0" {
			continue
		}

		remoteAddr := fmt.Sprintf("%s:%s", remoteHost, remotePort)
		b = buf[startIdx:]

		if remotePort == "53" {
			u.sshTun.CountStats(StatsDnsResolveCount, 1)
			if (cfg.DnsServers != nil) && len(cfg.DnsServers) > 0 {
				go func(req []byte) {
					for _, v := range cfg.DnsServers {
						err := u.sshTun.handleDNSrequest(u.inbound, addr, v+":53", req)
						if err != nil {
							continue
						}
						break
					}
				}([]byte(string(b)))
			} else {
				go u.sshTun.handleDNSrequest(u.inbound, addr, remoteAddr, []byte(string(b)))
			}
			continue
		}

		remoteAddr, ok := u.sshTun.isBypassProxy(remoteHost)
		if ok {
			go u.handleLocalProxyWrite([]byte(string(b)), addr, remoteAddr+":"+remotePort)
			continue
		}

		u.sshTun.CountStats(StatsUdpPktsSent, 1)
		//upsert ssh channel
		uc, err := u.getUDPChan(ctx, false)
		if err != nil {
			u.sshTun.CountStats(ErrorUdpRemoteProxy, 1)
			if strings.HasSuffix(err.Error(), "EOF") {
				continue
			}
			return u.Errorf("inbound-udpchan: %w", err)
		}
		//ignore link local udp packets
		if addr.Zone != "" && strings.HasPrefix(addr.String(), "169.254.") {
			continue
		}
		//send over channel, including source address
		if err := uc.Encode(addr.String(), remoteAddr, b); err != nil {
			u.sshTun.CountStats(ErrorUdpRemoteProxy, 1)
			if strings.HasSuffix(err.Error(), "EOF") {
				continue //dropped packet...
			}
			return u.Errorf("encode error: %w", err)
		}
		//stats
		atomic.AddInt64(&u.sent, int64(n))
		u.sshTun.CountStats(StatsBytesSent, int64(n))
	}
	return nil
}

func (u *udpListener) runOutBound(ctx context.Context) error {
	for !isDone(ctx) {
		//upsert ssh channel
		uc, err := u.getUDPChan(ctx, true)
		if err != nil {
			u.Debugf("[PROXY] [UDP] getUDPChan failed: %v", err)
			continue
		}
		//receive from channel, including source address
		pkt := chshare.UdpPacket{}
		if err := uc.Decode(&pkt); err != nil {
			u.Debugf("[PROXY] [UDP] decode error: %v", err)
			u.outboundMut.Lock()
			if u.outbound != nil && u.outbound.C != nil {
				u.outbound.C.Close()
			}
			u.outbound = nil
			u.outboundMut.Unlock()
			//wait 10ms before retry
			time.Sleep(time.Millisecond * 10)
			continue
		}

		u.sshTun.CountStats(StatsUdpPktsReceived, 1)
		//write back to inboud udp
		addr, err := net.ResolveUDPAddr("udp", pkt.Src)
		if err != nil {
			u.Debugf("[PROXY] [UDP] ResolveUDPAddr error: %v", err)
			continue
		}

		hostPort := strings.SplitN(pkt.Dst, ";", 2)
		if len(hostPort) != 2 {
			u.Debugf("[PROXY] [UDP] invalid pkt.Dst: %s", pkt.Dst)
		}
		if net.ParseIP(hostPort[0]) == nil {
			hostPort[0] = u.sshTun.resolveDomainToFake(hostPort[0] + ".")
		}
		dst := hostPort[0] + ":" + hostPort[1]
		n, err := WriteBackUDP(u.inbound, pkt.Payload, addr, dst)
		if err != nil {
			u.Debugf("[PROXY] [UDP] WriteBackUDP error: %v", err)
			continue
		}
		//stats
		atomic.AddInt64(&u.recv, int64(n))
		u.sshTun.CountStats(StatsBytesReceived, int64(n))
	}
	return nil
}

func (u *udpListener) getUDPChan(ctx context.Context, wait bool) (*chshare.UdpChannel, error) {
	sshConn := u.sshTun.getSSH(ctx, wait)
	if sshConn == nil {
		return nil, fmt.Errorf("ssh-conn nil")
	}

	u.outboundMut.Lock()
	defer u.outboundMut.Unlock()
	//cached
	if u.outbound != nil {
		return u.outbound, nil
	}
	//not cached, bind
	//ssh request for udp packets for this proxy's remote,
	//just "udp" since the remote address is sent with each packet
	rwc, reqs, err := sshConn.OpenChannel("tunnel", []byte("0.0.0.0:0/udp"))
	if err != nil {
		return nil, fmt.Errorf("ssh-chan error: %s", err)
	}
	go ssh.DiscardRequests(reqs)
	//remove on disconnect
	go u.unsetUDPChan(sshConn)
	//ready
	o := &chshare.UdpChannel{
		R: gob.NewDecoder(rwc),
		W: gob.NewEncoder(rwc),
		C: rwc,
	}
	u.outbound = o
	u.Debugf("[PROXY] [UDP] aquired channel")
	return o, nil
}

func (u *udpListener) unsetUDPChan(sshConn ssh.Conn) {
	sshConn.Wait()
	u.Debugf("[PROXY] [UDP] lost channel")
	u.outboundMut.Lock()
	u.outbound = nil
	u.outboundMut.Unlock()
}
