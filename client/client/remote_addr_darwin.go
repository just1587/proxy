// +build darwin

package main

import (
	"fmt"
	"net"
	"strings"
	"sync"
	"syscall"
	"time"

	chshare "github.com/jpillora/chisel/share"
	"github.com/miekg/dns"
)

var (
	intfIndex           = -1
	lastUpdateIndexTime int64
	intfLock            sync.Mutex
)

func getOriginalRemoteAddr(conn net.Conn, l *chshare.Logger) (string, string) {
	var host, port string
	portByte := make([]byte, 2)
	ipByte := make([]byte, 4)
	n, err := conn.Read(portByte)
	if err != nil {
		l.Debugf("[ORIGADDR] conn.Read port failed: %v", err)
		return host, port
	}
	if n != 2 {
		l.Debugf("[ORIGADDR] read port size not expect: %d", n)
		return host, port
	}

	n, err = conn.Read(ipByte)
	if err != nil {
		l.Debugf("[ORIGADDR] conn.Read ip failed: %v", err)
		return host, port
	}
	if n != 4 {
		l.Debugf("[ORIGADDR] getOriginalRemoteAddr read ip size not expect: %d", n)
		return host, port
	}

	host = fmt.Sprintf("%d.%d.%d.%d", ipByte[0], ipByte[1], ipByte[2], ipByte[3])
	port = fmt.Sprintf("%d", int(portByte[1])+int(portByte[0])<<8)
	return host, port
}

func getOriginalLocalAddr(conn net.Conn, l *chshare.Logger) (string, string) {
	return getOriginalRemoteAddr(conn, l)
}

func getOriginalRemoteAddrUDP(addr *net.UDPAddr, oob []byte, pkt []byte, l *chshare.Logger) (string, string, int) {
	if len(pkt) < 6 {
		return "", "", 0
	}
	host := fmt.Sprintf("%d.%d.%d.%d", pkt[2], pkt[3], pkt[4], pkt[5])
	port := fmt.Sprintf("%d", int(pkt[1])+int(pkt[0])<<8)
	return host, port, 6
}

func setTcpListeningSocketOption(listener *net.TCPListener) error {
	rawConn, err := listener.SyscallConn()
	if err != nil {
		return fmt.Errorf("conn SyscallConn: %w", err)
	}

	rawConn.Control(func(fdPrt uintptr) {
		fd := int(fdPrt)
		syscall.SetsockoptInt(fd, syscall.SOL_SOCKET, syscall.SO_REUSEADDR, 1)
	})
	return nil
}

func setUdpListeningSocketOption(listener *net.UDPConn) error {
	rawConn, err := listener.SyscallConn()
	if err != nil {
		return fmt.Errorf("conn SyscallConn: %w", err)
	}

	rawConn.Control(func(fdPrt uintptr) {
		fd := int(fdPrt)
		syscall.SetsockoptInt(fd, syscall.SOL_SOCKET, syscall.SO_REUSEADDR, 1)
	})
	return nil
}

func WriteBackUDP(conn *net.UDPConn, b []byte, addr *net.UDPAddr, remote string) (n int, err error) {
	return conn.WriteToUDP(b, addr)
}

func getInterfaceIndex() (int, error) {
	intfLock.Lock()
	defer intfLock.Unlock()
	if intfIndex == -1 || time.Now().Unix() > lastUpdateIndexTime+5 {
		iface, err := net.InterfaceByName(cfg.InterfaceName)
		if err != nil {
			return -1, err
		}
		lastUpdateIndexTime = time.Now().Unix()
		intfIndex = iface.Index
	}
	return intfIndex, nil
}

func makeMarkedDialer(address string, mark int) (*net.Dialer, error) {
	if strings.Contains(address, ".") {
		ss := strings.Split(address, ":")
		address = ss[0]
	}

	idx, err := getInterfaceIndex()
	if err != nil {
		return nil, err
	}

	return &net.Dialer{
		Control: func(network, address string, c syscall.RawConn) error {
			return c.Control(func(fd uintptr) {
				syscall.SetsockoptInt(int(fd), syscall.IPPROTO_IP, syscall.IP_BOUND_IF, idx)
			})
		},
	}, nil
}

func resolveIP(name string, dnsServer string, timeout time.Duration) ([]string, error) {
	addrs := []string{}
	parsed := net.ParseIP(name)
	if parsed != nil {
		addrs = append(addrs, name)
		return addrs, nil
	}

	var err error
	c := new(dns.Client)

	c.Dialer, err = makeMarkedDialer(dnsServer, cfg.SocketMarkValue)
	if err != nil {
		return nil, err
	}
	c.Dialer.Timeout = time.Second
	c.Net = "udp"
	m := new(dns.Msg)
	m.SetQuestion(dns.Fqdn(name), dns.TypeA)

	inA, _, err := c.Exchange(m, dnsServer+":53")
	if err != nil {
		return nil, err
	}

	for _, record := range inA.Answer {
		if t, ok := record.(*dns.A); ok {
			addrs = append(addrs, t.A.String())
		}
	}

	return addrs, nil
}

func websocketDialer(network, addr string) (net.Conn, error) {
	return dialBypassProxy(network, addr)
}

func dialBypassProxy(network, address string) (net.Conn, error) {
	hostPort := strings.Split(address, ":")
	if len(hostPort) != 2 {
		return nil, fmt.Errorf("invalid address")
	}

	var addrs []string
	for _, dnsSrv := range cfg.DnsServers {
		var err error
		addrs, err = resolveIP(hostPort[0], dnsSrv, time.Second)
		if err != nil {
			thisClient.Debugf("[DNS] resolveIP failed: %v, domain: %v, dns: %v", err, hostPort[0], dnsSrv)
			continue
		}
		if len(addrs) == 0 {
			thisClient.Debugf("[DNS] resolveIP return empty record, domain: %v, dns: %v", hostPort[0], dnsSrv)
			continue
		}

		var dialer *net.Dialer
		dialer, err = makeMarkedDialer(addrs[0], cfg.SocketMarkValue)
		if err != nil {
			return nil, err
		}
		return dialer.Dial(network, addrs[0]+":"+hostPort[1])
	}

	return nil, fmt.Errorf("dns resolve error: empty dns records, dns server: %v", cfg.DnsServers)
}
