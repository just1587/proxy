// +build windows

package main

import (
	"context"
	"encoding/binary"
	"fmt"
	"net"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
	"unsafe"

	chshare "github.com/jpillora/chisel/share"
	"github.com/miekg/dns"
)

var (
	mmapAddr       = uintptr(0)
	mmapBlockSize  = 28 //unsafe.Sizeof(*syscall.RawSockaddrInet6)
	mmapBlockCount = 65536
)

func getOriginalRemoteAddr(conn net.Conn, l *chshare.Logger) (string, string) {
	var host, port string

	if mmapAddr == uintptr(0) {
		file, err := syscall.UTF16PtrFromString("Global\\CHISELTCPCONNMAP")
		if err != nil {
			l.Debugf("syscall.UTF16PtrFromString failed: %v", err)
			return host, port
		}
		size := mmapBlockSize * mmapBlockCount
		handle, err := syscall.CreateFileMapping(0, nil, syscall.PAGE_READONLY, 0, uint32(size), file)
		if err != nil {
			l.Debugf("syscall.CreateFileMapping failed: %v", err)
			return host, port
		}
		defer syscall.CloseHandle(handle)

		mmapAddr, err = syscall.MapViewOfFile(handle, syscall.FILE_MAP_READ, 0, 0, 0)
		if err != nil {
			mmapAddr = uintptr(0)
			l.Debugf("syscall.MapViewOfFile failed: %v", err)
			return host, port
		}
		//defer syscall.UnmapViewOfFile(mapBuffer)
	}

	_, remotePort, _ := net.SplitHostPort(conn.RemoteAddr().String())
	u64, _ := strconv.ParseUint(remotePort, 10, 16)
	remotePortInt := uint16(u64)

	addr := (*syscall.RawSockaddrInet6)(unsafe.Pointer(uintptr(unsafe.Pointer(mmapAddr)) + (uintptr(remotePortInt) * uintptr(mmapBlockSize))))

	pp := (*syscall.RawSockaddrInet4)(unsafe.Pointer(addr))
	p := (*[2]byte)(unsafe.Pointer(&pp.Port))
	ip := net.IPv4(pp.Addr[0], pp.Addr[1], pp.Addr[2], pp.Addr[3])
	host = ip.String()
	port = fmt.Sprintf("%d", int(p[0])<<8+int(p[1]))
	return host, port
}

func getOriginalLocalAddr(conn net.Conn, l *chshare.Logger) (string, string) {
	srcAddr := conn.RemoteAddr().String()
	srcHostPort := strings.SplitN(srcAddr, ":", 2)
	if len(srcHostPort) != 2 {
		return "", ""
	}
	return srcHostPort[0], srcHostPort[1]
}

func getOriginalRemoteAddrUDP(addr *net.UDPAddr, oob []byte, pkt []byte, l *chshare.Logger) (string, string, int) {
	var host, port string
	var addrV6 *syscall.RawSockaddrInet6

	paddingLen := int(unsafe.Sizeof(*addrV6))
	if len(pkt) < paddingLen {
		return "", "", 0
	}

	addrV6 = (*syscall.RawSockaddrInet6)(unsafe.Pointer(&pkt[0]))
	pp := (*syscall.RawSockaddrInet4)(unsafe.Pointer(&pkt[0]))
	p := (*[2]byte)(unsafe.Pointer(&pp.Port))
	ip := net.IPv4(pp.Addr[0], pp.Addr[1], pp.Addr[2], pp.Addr[3])
	host = ip.String()
	port = fmt.Sprintf("%d", int(p[0])<<8+int(p[1]))

	return host, port, paddingLen
}

func setTcpListeningSocketOption(listener *net.TCPListener) error {
	rawConn, err := listener.SyscallConn()
	if err != nil {
		return fmt.Errorf("conn SyscallConn: %w", err)
	}

	rawConn.Control(func(fdPrt uintptr) {
		fd := syscall.Handle(fdPrt)
		syscall.SetsockoptInt(fd, syscall.SOL_SOCKET, syscall.SO_REUSEADDR, 1)
	})
	return nil
}

func setUdpListeningSocketOption(listener *net.UDPConn) error {
	rawConn, err := listener.SyscallConn()
	if err != nil {
		return fmt.Errorf("conn SyscallConn: %w", err)
	}

	rawConn.Control(func(fdPrt uintptr) {
		fd := syscall.Handle(fdPrt)
		syscall.SetsockoptInt(fd, syscall.SOL_SOCKET, syscall.SO_REUSEADDR, 1)
	})
	return nil
}

func WriteBackUDP(conn *net.UDPConn, b []byte, addr *net.UDPAddr, remote string) (n int, err error) {
	var addrV6 *syscall.RawSockaddrInet6
	paddingLen := int(unsafe.Sizeof(*addrV6))
	header := make([]byte, paddingLen)
	addr_str, port_str, err := net.SplitHostPort(remote)
	if err != nil {
		return 0, err
	}

	port, err := strconv.Atoi(port_str)
	if err != nil {
		return 0, err
	}

	ip := net.ParseIP(addr_str)
	if ip == nil {
		return 0, nil
	}
	ip = ip.To4()
	if ip == nil {
		return 0, nil
	}
	ipInt0 := uint(ip[3]) | uint(ip[2])<<8 | uint(ip[1])<<16 | uint(ip[0])<<24
	binary.LittleEndian.PutUint16(header[0:2], uint16(syscall.AF_INET))
	binary.BigEndian.PutUint16(header[2:4], uint16(port))
	binary.BigEndian.PutUint32(header[4:8], uint32(ipInt0))
	binary.BigEndian.PutUint32(header[8:12], uint32(0))
	binary.BigEndian.PutUint32(header[12:16], uint32(0))

	pkt := append(header[:], b...)
	n, err = conn.WriteToUDP(pkt, addr)
	return n - paddingLen, err
}

func makeMarkedDialer(address string, mark int) (*net.Dialer, error) {
	return &net.Dialer{}, nil
}

func websocketDialer(network, addr string) (net.Conn, error) {
	return dialBypassProxy(network, addr)
}

const MAX_HOSTNAME_LEN = 128
const MAX_DOMAIN_NAME_LEN = 128
const MAX_SCOPE_ID_LEN = 256
const ERROR_BUFFER_OVERFLOW = 111

type FIXED_INFO_W2KSP1 struct {
	HostName         [MAX_HOSTNAME_LEN + 4]byte
	DomainName       [MAX_DOMAIN_NAME_LEN + 4]byte
	CurrentDnsServer *syscall.IpAddrString
	DnsServerList    syscall.IpAddrString
	NodeType         uint32
	ScopeId          [MAX_SCOPE_ID_LEN + 4]byte
	EnableRouting    uint32
	EnableProxy      uint32
	EnableDns        uint32
}

var (
	localDNSServers   []string
	lastUpdateDNSTime int64
	dnsLock           sync.Mutex
)

func getLocalDNSServers() ([]string, error) {
	var ret []string
	h, err := syscall.LoadLibrary("iphlpapi.dll")
	if err != nil {
		return ret, err
	}
	defer syscall.FreeLibrary(h)

	GetNetworkParams, err := syscall.GetProcAddress(h, "GetNetworkParams")
	if err != nil {
		return ret, err
	}

	var size int
	buffer := make([]byte, 1)
	r, _, _ := syscall.Syscall(GetNetworkParams, 2,
		uintptr(unsafe.Pointer(&buffer[0])), uintptr(unsafe.Pointer(&size)), 0)
	if r != ERROR_BUFFER_OVERFLOW {
		return ret, fmt.Errorf("GetNetworkParams failed: %d", r)
	}

	buffer = make([]byte, size)
	r, _, _ = syscall.Syscall(GetNetworkParams, 2,
		uintptr(unsafe.Pointer(&buffer[0])), uintptr(unsafe.Pointer(&size)), 0)
	if r != 0 {
		return ret, fmt.Errorf("GetNetworkParams failed: %d", r)
	}
	info := (*FIXED_INFO_W2KSP1)(unsafe.Pointer(&buffer[0]))
	for ai := &info.DnsServerList; ai != nil; ai = ai.Next {
		d := strings.TrimRight(string(ai.IpAddress.String[:]), "\x00")
		ret = append(ret, d)
	}
	return ret, nil
}

func getCachedLocalDNSServer() []string {
	dnsLock.Lock()
	defer dnsLock.Unlock()

	if len(localDNSServers) == 0 || time.Now().Unix() > lastUpdateDNSTime+5 {
		dnss, err := getLocalDNSServers()
		if err != nil {
			return []string{"8.8.8.8", "8.8.4.4"}
		}
		localDNSServers = dnss
		lastUpdateDNSTime = time.Now().Unix()
	}
	return localDNSServers
}

func resolveIP(name string, dnsServer string, timeout time.Duration) ([]string, error) {
	ctx, cancelfunc := context.WithTimeout(context.Background(), timeout)
	defer cancelfunc()

	addrs := []string{}
	parsed := net.ParseIP(name)
	if parsed != nil {
		addrs = append(addrs, name)
		return addrs, nil
	}

	c := new(dns.Client)
	c.Net = "udp"
	m := new(dns.Msg)
	m.SetQuestion(dns.Fqdn(name), dns.TypeA)

	inA, _, err := c.ExchangeContext(ctx, m, dnsServer+":53")
	if err != nil {
		return nil, err
	}

	for _, record := range inA.Answer {
		if t, ok := record.(*dns.A); ok {
			addrs = append(addrs, t.A.String())
		}
	}

	return addrs, nil
}

func dialBypassProxy(network, address string) (net.Conn, error) {
	hostPort := strings.Split(address, ":")
	if len(hostPort) != 2 {
		return nil, fmt.Errorf("invalid address")
	}

	var addrs []string
	localDnsSrvs := getCachedLocalDNSServer()
	for _, dnsSrv := range localDnsSrvs {
		var err error
		addrs, err = resolveIP(hostPort[0], dnsSrv, time.Second)
		if err != nil {
			thisClient.Debugf("[DNS] resolveIP failed: %v, domain: %v, dns: %v", err, hostPort[0], dnsSrv)
			continue
		}
		if len(addrs) == 0 {
			thisClient.Debugf("[DNS] resolveIP return empty record, domain: %v, dns: %v", hostPort[0], dnsSrv)
			continue
		}
		return net.Dial(network, addrs[0]+":"+hostPort[1])
	}

	return nil, fmt.Errorf("dns resolve error: empty dns records, dns server: %v", localDnsSrvs)
}
