package main

/*
#cgo CFLAGS: -I .
#cgo LDFLAGS: -L .
#include <stdlib.h>
*/
import "C"
import (
	"context"
	"encoding/json"
	"fmt"
	_ "net/http/pprof"
	"os/signal"
	"runtime"
	"syscall"
	"time"
	"unsafe"

	chshare "github.com/jpillora/chisel/share"
)

//export StartTunnel
func StartTunnel(cfgStr *C.char, success *C.int) *C.char {
	err := validConfig(C.GoString(cfgStr))
	if err != nil {
		*success = -1
		return C.CString(fmt.Sprintf("parse config string error: %s", err.Error()))
	}

	signal.Ignore(syscall.SIGPIPE)

	remotes := []string{cfg.Remote}
	if cfg.RemoteUDP != "" {
		remotes = append(remotes, cfg.RemoteUDP+"/udp")
	}
	thisClient, err = NewClient(&Config{
		Fingerprint:         cfg.FingerPrint,
		CAFile:              cfg.CaFile,
		KeepAlive:           time.Duration(cfg.KeepAlive) * time.Second,
		MaxRetryCount:       cfg.MaxRetryCount,
		MaxRetryInterval:    time.Duration(cfg.MaxRetryInterval) * time.Second,
		ServerList:          cfg.ServerList,
		SkipTlsVerification: cfg.SkipTlsVerification,
		Remotes:             remotes,
		HostHeader:          cfg.Hostname,
		Debug:               cfg.Verbose,
	})
	if err != nil {
		*success = -1
		return C.CString(err.Error())
	}
	if cfg.Pid {
		generatePidFile()
	}
	if err := thisClient.Start(context.Background()); err != nil {
		*success = -1
		return C.CString(err.Error())
	}
	*success = 0
	return C.CString("")
}

//export WaitTunnel
func WaitTunnel() *C.char {
	if thisClient == nil {
		return C.CString("tunnel not started")
	}
	err := thisClient.Wait()
	if err != nil {
		return C.CString(err.Error())
	}
	return C.CString("")
}

//export CloseTunnel
func CloseTunnel() {
	if thisClient == nil {
		return
	}
	thisClient.Close()
	thisClient = nil
}

//export StatusTunnel
func StatusTunnel() *C.char {
	if thisClient == nil {
		return C.CString("tunnel not started")
	}
	stats := thisClient.tunnel.getTunnelStatus()
	return C.CString(stats)
}

//export SetTunnelDnsDomains
func SetTunnelDnsDomains(jsonStr *C.char, success *C.int) *C.char {
	var ddc dnsDomainCfg
	content := C.GoString(jsonStr)
	err := json.Unmarshal([]byte(content), &ddc)
	*success = -1
	if err != nil {
		return C.CString(fmt.Sprintf("json unmarshal failed: %v", err))
	}
	if thisClient == nil {
		return C.CString("tunnel not started")
	}

	thisClient.tunnel.dns.UpdateUserConfig(ddc.PaDomains)
	thisClient.tunnel.dns.UpdateUserConfigNets(ddc.PaNets)
	thisClient.tunnel.updateBypassDomains(ddc.DnsBypassDomains)

	*success = 0
	return C.CString("")
}

//export SetDnsServers
func SetDnsServers(jsonStr *C.char) *C.char {
	var dnsServers []string
	content := C.GoString(jsonStr)
	err := json.Unmarshal([]byte(content), &dnsServers)
	if err != nil {
		return C.CString(fmt.Sprintf("json unmarshal failed: %v", err))
	}
	cfg.DnsServers = dnsServers
	return C.CString("")
}

//export GetTunnelVersion
func GetTunnelVersion() *C.char {
	versionStr := fmt.Sprintf("{\"os\":\"%s-%s\",\"version\":\"%s-%s\",\"build_time\":\"%s\"}", runtime.GOOS,
		runtime.GOARCH, chshare.BuildVersion, buildGitRevison, buildTimestamp)
	return C.CString(versionStr)
}

//export FreeTunnelErrMem
func FreeTunnelErrMem(mem *C.char) {
	if mem != nil {
		C.free(unsafe.Pointer(mem))
	}
}
