package main

import (
	"io"
	"net"
	"os"
	"strings"
	"sync"
	"time"

	chshare "github.com/jpillora/chisel/share"
)

const localProxyMaxConns = 1024

type udpConn struct {
	id string
	net.Conn
}

type udpConns struct {
	*chshare.Logger
	sync.Mutex
	m map[string]*udpConn
}

func (u *udpListener) handleLocalProxyWrite(pkt []byte, addr *net.UDPAddr, remote string) {
	conn, exists, err := u.localUdpConns.dial(addr.String(), remote, u.Logger)
	if err != nil {
		u.sshTun.CountStats(ErrorUdpLocalProxy, 1)
		u.Debugf("[BYPASS] [UDP] handleLocalProxyWrite error: %v, local %s, remote %s", err, addr.String(), remote)
		return
	}

	if !exists {
		if u.localUdpConns.len() <= localProxyMaxConns {
			go u.handleLocalProxyRead(addr, conn, remote)
		} else {
			u.sshTun.CountStats(ErrorUdpLocalProxy, 1)
			u.localUdpConns.remove(conn.id)
			u.Debugf("[BYPASS] [UDP] exceeded max udp connections (%d), ignore %s->%s", localProxyMaxConns, addr.String(), remote)
		}
	}
	_, _ = conn.Write(pkt)
}

func (u *udpListener) handleLocalProxyRead(addr *net.UDPAddr, conn *udpConn, remote string) {
	defer func() {
		u.localUdpConns.remove(conn.id)
	}()

	const maxMTU = 9012
	buf := make([]byte, maxMTU)
	for {
		//response must arrive within 15 seconds
		const deadline = 15 * time.Second
		conn.SetReadDeadline(time.Now().Add(deadline))
		//read response
		n, err := conn.Read(buf)
		if err != nil {
			if !os.IsTimeout(err) && err != io.EOF {
				u.Debugf("[BYPASS] [UDP] read error: %s, local %s, remote %s", err, addr.String(), remote)
			}
			break
		}
		b := buf[:n]
		//encode back over ssh connection
		hostPort := strings.SplitN(remote, ":", 2)
		if len(hostPort) != 2 {
			u.sshTun.CountStats(ErrorUdpLocalProxy, 1)
			u.Debugf("[BYPASS] [UDP] handleLocalProxyRead invalid remote: %s", remote)
		}
		if net.ParseIP(hostPort[0]) == nil {
			hostPort[0] = u.sshTun.resolveDomainToFake(hostPort[0] + ".")
		}
		remote = hostPort[0] + ":" + hostPort[1]
		_, err = WriteBackUDP(u.inbound, b, addr, remote)
		if err != nil {
			u.sshTun.CountStats(ErrorUdpLocalProxy, 1)
			u.Debugf("[BYPASS] [UDP] WriteToUDP error: %s, local %s, remote %s", err, addr.String(), remote)
			return
		}
	}
}

func (cs *udpConns) dial(id, addr string, l *chshare.Logger) (*udpConn, bool, error) {
	cs.Lock()
	conn, ok := cs.m[id]
	cs.Unlock()
	if !ok {
		c, err := dialBypassProxy("udp", addr)
		if err != nil {
			return nil, false, err
		}
		conn = &udpConn{
			id:   id,
			Conn: c,
		}
		cs.Lock()
		cs.m[id] = conn
		cs.Unlock()
	}
	return conn, ok, nil
}

func (cs *udpConns) len() int {
	cs.Lock()
	l := len(cs.m)
	cs.Unlock()
	return l
}

func (cs *udpConns) remove(id string) {
	cs.Lock()
	delete(cs.m, id)
	cs.Unlock()
}

func (cs *udpConns) closeAll() {
	cs.Lock()
	for id, conn := range cs.m {
		conn.Close()
		delete(cs.m, id)
	}
	cs.Unlock()
}
