package main

import (
	"fmt"
	"net"
	"strings"
	"sync"
	"time"

	chshare "github.com/jpillora/chisel/share"
	"github.com/miekg/dns"
	"github.com/patrickmn/go-cache"
)

func inetNtoa(ip uint32) net.IP {
	var bytes [4]byte
	bytes[0] = byte(ip & 0xFF)
	bytes[1] = byte((ip >> 8) & 0xFF)
	bytes[2] = byte((ip >> 16) & 0xFF)
	bytes[3] = byte((ip >> 24) & 0xFF)
	return net.IPv4(bytes[3], bytes[2], bytes[1], bytes[0])
}

func inetAton(ip string) uint32 {
	ipByte := net.ParseIP(ip).To4()
	if ipByte == nil {
		return 0
	}
	return uint32(ipByte[0])<<24 + uint32(ipByte[1])<<16 + uint32(ipByte[2])<<8 + uint32(ipByte[3])
}

type intQueue struct {
	stack []uint32
}

func NewIntQueue(cnt uint32) *intQueue {
	return &intQueue{
		stack: make([]uint32, 0, cnt),
	}
}

func (q *intQueue) Push(item uint32) {
	q.stack = append(q.stack, item)
}

func (q *intQueue) Pop() (uint32, error) {
	qLen := len(q.stack)
	if qLen > 0 {
		retVal := q.stack[0]
		q.stack = q.stack[1:]
		return retVal, nil
	}
	return 0, fmt.Errorf("queue is empty")
}

func (q *intQueue) Size() int {
	return len(q.stack)
}

func (q *intQueue) Empty() bool {
	return len(q.stack) == 0
}

type dnsFakeResolverConfig struct {
	*chshare.Logger
	startIp uint32
	ipCount uint32
}

type dnsFakeResolver struct {
	sync.RWMutex
	*chshare.Logger
	startIp    uint32
	ipCount    uint32
	queue      *intQueue
	cache      *cache.Cache      //dns to ip
	ipMap      map[uint32]string //ip to dns
	userConfig map[string]bool
	userNets   []string
}

func NewDnsFakeResolver(c *dnsFakeResolverConfig) *dnsFakeResolver {
	d := &dnsFakeResolver{
		Logger:     c.Logger.Fork("dns"),
		queue:      NewIntQueue(c.ipCount),
		cache:      cache.New(24*time.Hour, 10*time.Second),
		ipMap:      make(map[uint32]string),
		userConfig: make(map[string]bool),
		startIp:    c.startIp,
		ipCount:    c.ipCount,
	}
	d.cache.OnEvicted(d.OnEvicted)

	for i := c.startIp; i < c.ipCount+c.startIp; i++ {
		d.queue.Push(i)
	}

	return d
}

func (d *dnsFakeResolver) OnEvicted(k string, v interface{}) {
	d.Lock()
	defer d.Unlock()
	ip := v.(uint32)
	delete(d.ipMap, ip)
	d.queue.Push(ip)
}

func (d *dnsFakeResolver) Lookup(domain string) string {
	d.Lock()
	defer d.Unlock()
	if ip, ok := d.cache.Get(domain); ok {
		d.cache.SetDefault(domain, ip.(uint32))
		return inetNtoa(ip.(uint32)).String()
	}

	ip, err := d.queue.Pop()
	if err != nil {
		//no available ip
		d.Debugf("[DNS] no available ip, domain %s", domain)
		return "0.0.0.0"
	}

	d.ipMap[ip] = domain
	d.cache.Set(domain, ip, cache.DefaultExpiration)
	return inetNtoa(ip).String()
}

func (d *dnsFakeResolver) ReverseLookup(ip string) (string, bool) {
	d.Lock()
	defer d.Unlock()
	ipInt := inetAton(ip)
	domain, ok := d.ipMap[ipInt]
	return domain, ok
}

func (d *dnsFakeResolver) UpdateUserConfig(domainList []string) error {
	d.Lock()
	defer d.Unlock()
	d.userConfig = make(map[string]bool)
	for _, domain := range domainList {
		if domain == "" {
			continue
		}
		if !strings.HasSuffix(domain, ".") {
			domain = domain + "."
		}
		d.userConfig[domain] = true
	}
	return nil
}

func (d *dnsFakeResolver) MatchUserConfig(domain string) bool {
	d.RLock()
	defer d.RUnlock()

	if _, ok := d.userConfig[domain]; ok {
		return true
	}

	arr := strings.Split(domain, ".")
	arrLen := len(arr)
	for i := 0; i < arrLen-1; i++ {
		wildcard := "*." + strings.Join(arr[i+1:], ".")
		if _, ok := d.userConfig[wildcard]; ok {
			return true
		}
	}

	return false
}

func (d *dnsFakeResolver) UpdateUserConfigNets(netsList []string) error {
	d.Lock()
	defer d.Unlock()
	d.userNets = nil
	for _, nets := range netsList {
		_, _, err := net.ParseCIDR(nets)
		if err != nil {
			d.Debugf("[DNS] skip invalid user nets config: %v, err: %v", nets, err)
			continue
		}
		d.userNets = append(d.userNets, nets)
	}
	return nil
}

func (d *dnsFakeResolver) getCachedCount() int {
	d.RLock()
	defer d.RUnlock()
	return d.cache.ItemCount()
}

func (d *dnsFakeResolver) getUserConfigedDomains() []string {
	d.RLock()
	defer d.RUnlock()
	ret := make([]string, 0, len(d.userConfig))
	for k := range d.userConfig {
		ret = append(ret, k)
	}
	return ret
}

func (d *dnsFakeResolver) getUserConfigedNets() []string {
	d.RLock()
	defer d.RUnlock()
	ret := make([]string, 0, len(d.userNets))
	ret = d.userNets
	return ret
}

func (d *dnsFakeResolver) isFakeIP(ip string) bool {
	ipInt := inetAton(ip)
	if ipInt >= d.startIp && ipInt < d.startIp+d.ipCount {
		return true
	}
	return false
}

func (t *Tunnel) updateBypassDomains(domains []string) error {
	t.Lock()
	defer t.Unlock()
	t.bypassDns = nil
	for _, domain := range domains {
		if domain == "" {
			continue
		}
		if !strings.HasSuffix(domain, ".") {
			domain = domain + "."
		}
		t.bypassDns = append(t.bypassDns, domain)

	}
	return nil
}

func (t *Tunnel) getBypassDns() []string {
	t.RLock()
	defer t.RUnlock()
	ret := make([]string, 0, len(t.bypassDns))
	for _, k := range t.bypassDns {
		ret = append(ret, k)
	}
	return ret
}

func (t *Tunnel) isDnsNeedBypass(domain string) bool {
	t.RLock()
	defer t.RUnlock()
	for _, v := range t.bypassDns {
		if v == domain || strings.HasSuffix(domain, v) {
			return true
		}
	}

	return false
}

func (t *Tunnel) newDirectDnsRequest(req *dns.Msg, remoteAddr string) (*dns.Msg, error) {
	var err error
	c := new(dns.Client)
	c.Net = "udp"

	addr_str, _, err := net.SplitHostPort(remoteAddr)
	if err != nil {
		t.Debugf("[DNS] SplitHostPort failed: %v, request %v, dns %s", err, req.Question, remoteAddr)
		return nil, fmt.Errorf("[DNS] SplitHostPort failed: %v, req: %+v", err, req.Question)
	}

	c.Dialer, err = makeMarkedDialer(addr_str, cfg.SocketMarkValue)
	if err != nil {
		t.Debugf("[DNS] makeMarkedDialer failed: %v, request %s, dns %s", err, req.Question, remoteAddr)
		return nil, fmt.Errorf("[DNS] makeMarkedDialer failed: %w, req: %+v", err, req.Question)
	}
	resp, _, err := c.Exchange(req, remoteAddr)
	if err != nil {
		t.Debugf("[DNS] resolve tunnel server %v via %s failed: %v", req.Question, remoteAddr, err)
		return nil, fmt.Errorf("[DNS] dns Exchange failed: %w, req: %+v", err, req.Question)
	}
	t.Debugf("[DNS] [%+v] resolved to [%+v] via %s", strings.Join(strings.Fields(req.String()), " "),
		strings.Join(strings.Fields(resp.String()), " "), remoteAddr)
	return resp, nil
}

func (t *Tunnel) handleDNSrequest(conn *net.UDPConn, addr *net.UDPAddr, remoteAddr string, pkt []byte) error {
	defer func() {
		err := recover()
		if err != nil {
			t.Debugf("handleDNSrequest recover from error: %v", err)
		}
	}()

	var resp *dns.Msg
	req := &dns.Msg{}
	err := req.Unpack(pkt)
	if err != nil {
		t.Debugf("[DNS] unpack dns request failed: %v", err)
		return fmt.Errorf("handleDNSrequest req.Unpack failed: %w", err)
	}

	var ip net.IP
	if len(req.Question) == 1 && req.Question[0].Qtype == dns.TypeA && !t.isDnsNeedBypass(req.Question[0].Name) {
		if t.dns.MatchUserConfig(req.Question[0].Name) {
			resp = &dns.Msg{}
			resp.SetReply(req)
			resp.Authoritative = true
			ip = net.ParseIP(t.resolveDomainToFake(resp.Question[0].Name))
			resp.Answer = append(resp.Answer, &dns.A{
				Hdr: dns.RR_Header{
					Name:   resp.Question[0].Name,
					Rrtype: dns.TypeA,
					Class:  dns.ClassINET,
					Ttl:    1,
				},
				A: ip,
			})
			t.Debugf("[DNS] %s resolved to %s", resp.Question[0].Name, ip.String())
		} else {
			resp, err = t.newDirectDnsRequest(req, remoteAddr)
			if err != nil {
				return err
			}
		}
	} else if len(req.Question) == 1 && req.Question[0].Qtype == dns.TypeAAAA {
		resp = &dns.Msg{}
		resp.SetReply(req)
		resp.Authoritative = true
		t.Debugf("[DNS] skip %s AAAA request", resp.Question[0].Name)
	} else {
		if len(req.Question) == 1 && req.Question[0].Qtype == dns.TypeA && t.isDnsNeedBypass(req.Question[0].Name) {
		}
		resp, err = t.newDirectDnsRequest(req, remoteAddr)
		if err != nil {
			return err
		}
	}

	data, err := resp.Pack()
	if err != nil {
		t.Debugf("[DNS] pack dns %v response failed: %v", req.Question, err)
		return fmt.Errorf("handleDNSrequest resp.Unpack failed: %w", err)
	}

	_, err = WriteBackUDP(conn, data, addr, remoteAddr)
	if err != nil {
		t.Debugf("[DNS] WriteBackUDP response failed: %v, req %+v", err, req.Question)
		return fmt.Errorf("handleDNSrequest WriteBackUDP failed: %w", err)
	}
	return nil
}

func (t *Tunnel) tagIpProxyFlag(ip string) bool {
	netIp := net.ParseIP(ip)
	if netIp == nil {
		return false
	}

	t.dns.RLock()
	defer t.dns.RUnlock()

	for _, nets := range t.dns.userNets {
		_, pri, err := net.ParseCIDR(nets)
		if err != nil {
			continue
		}
		if pri.Contains(netIp) {
			return true
		}
	}
	return false
}

func (t *Tunnel) resolveDomainToFake(domain string) string {
	if t.dns.MatchUserConfig(domain) {
		return t.dns.Lookup(domain)
	}
	return ""
}

func (t *Tunnel) isBypassProxy(ip string) (string, bool) {
	var domainName string
	var isProxyDomain bool

	if t.dns.isFakeIP(ip) {
		domainName, isProxyDomain = t.dns.ReverseLookup(ip)
		domainLen := len(domainName)
		if isProxyDomain {
			return domainName[:domainLen-1], false
		}
		return domainName[:domainLen-1], true
	} else {
		return ip, !t.tagIpProxyFlag(ip)
	}
}
