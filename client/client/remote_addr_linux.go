// +build linux

package main

import (
	"bytes"
	"context"
	"encoding/binary"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"syscall"
	"unsafe"

	chshare "github.com/jpillora/chisel/share"
)

func setTcpListeningSocketOption(listener *net.TCPListener) error {
	rawConn, err := listener.SyscallConn()
	if err != nil {
		return fmt.Errorf("conn SyscallConn: %w", err)
	}

	rawConn.Control(func(fdPrt uintptr) {
		fd := int(fdPrt)
		syscall.SetsockoptInt(fd, syscall.SOL_SOCKET, syscall.SO_REUSEADDR, 1)
		syscall.SetsockoptInt(fd, syscall.SOL_IP, syscall.IP_TRANSPARENT, 1)
	})
	return nil
}

func setUdpListeningSocketOption(listener *net.UDPConn) error {
	rawConn, err := listener.SyscallConn()
	if err != nil {
		return fmt.Errorf("conn SyscallConn: %w", err)
	}

	rawConn.Control(func(fdPrt uintptr) {
		fd := int(fdPrt)
		syscall.SetsockoptInt(fd, syscall.SOL_SOCKET, syscall.SO_REUSEADDR, 1)
		syscall.SetsockoptInt(fd, syscall.SOL_IP, syscall.IP_TRANSPARENT, 1)
		syscall.SetsockoptInt(fd, syscall.SOL_IP, syscall.IP_RECVORIGDSTADDR, 1)
	})
	return nil
}

func getOriginalRemoteAddr(conn net.Conn, l *chshare.Logger) (string, string) {
	c, ok := conn.(*net.TCPConn)
	if !ok {
		return "", ""
	}
	rc, err := c.SyscallConn()
	if err != nil {
		return "", ""
	}

	var addr *net.TCPAddr
	rc.Control(func(fd uintptr) {
		addr, err = getOrigDstFromSocksOpt(fd)
	})

	remoteAddr := addr.String()
	hostPort := strings.Split(remoteAddr, ":")
	return hostPort[0], hostPort[1]
}

func getOriginalLocalAddr(conn net.Conn, l *chshare.Logger) (string, string) {
	srcAddr := conn.RemoteAddr().String()
	srcHostPort := strings.SplitN(srcAddr, ":", 2)
	if len(srcHostPort) != 2 {
		return "", ""
	}
	return srcHostPort[0], srcHostPort[1]
}

func getOrigDstFromSocksOpt(fd uintptr) (*net.TCPAddr, error) {
	const SO_ORIGINAL_DST = 80
	raw := syscall.RawSockaddrInet4{}
	siz := unsafe.Sizeof(raw)
	if err := getSocksOpt(fd, syscall.IPPROTO_IP, SO_ORIGINAL_DST, uintptr(unsafe.Pointer(&raw)), uintptr(unsafe.Pointer(&siz))); err != nil {
		return nil, err
	}

	var addr net.TCPAddr
	addr.IP = net.IPv4(raw.Addr[0], raw.Addr[1], raw.Addr[2], raw.Addr[3])
	p := (*[2]byte)(unsafe.Pointer(&raw.Port))
	addr.Port = int(p[0])<<8 + int(p[1])
	return &addr, nil
}

func getSocksOpt(s uintptr, level int, name int, val uintptr, vallen uintptr) (err error) {
	_, _, e1 := syscall.Syscall6(syscall.SYS_GETSOCKOPT, s, uintptr(level), uintptr(name), val, vallen, 0)
	if e1 != 0 {
		err = e1
	}
	return
}

func getOriginalRemoteAddrUDP(addr *net.UDPAddr, oob []byte, pkt []byte, l *chshare.Logger) (string, string, int) {
	msgs, err := syscall.ParseSocketControlMessage(oob)
	if err != nil {
		return "", "", 0
	}

	var originalDst *net.UDPAddr
	for _, msg := range msgs {
		if msg.Header.Level == syscall.SOL_IP && msg.Header.Type == syscall.IP_RECVORIGDSTADDR {
			originalDstRaw := &syscall.RawSockaddrInet4{}
			if err = binary.Read(bytes.NewReader(msg.Data), binary.LittleEndian, originalDstRaw); err != nil {
				return "", "", 0
			}

			if originalDstRaw.Family == syscall.AF_INET {
				pp := (*syscall.RawSockaddrInet4)(unsafe.Pointer(originalDstRaw))
				p := (*[2]byte)(unsafe.Pointer(&pp.Port))
				originalDst = &net.UDPAddr{
					IP:   net.IPv4(pp.Addr[0], pp.Addr[1], pp.Addr[2], pp.Addr[3]),
					Port: int(p[0])<<8 + int(p[1]),
				}
			}
		}
	}

	if originalDst == nil {
		return "", "", 0
	}

	remoteAddr := originalDst.String()
	hostPort := strings.Split(remoteAddr, ":")
	return hostPort[0], hostPort[1], 0
}

func WriteBackUDP(conn *net.UDPConn, b []byte, addr *net.UDPAddr, remote string) (n int, err error) {
	lAddr, err := stringToUdpAddr(remote)
	if err != nil {
		n = 0
		return
	}
	tc, err := dialUDP("udp", lAddr, addr)
	if err != nil {
		n = 0
		return
	}
	n, err = tc.Write(b)
	tc.Close()
	return
}

func dialUDP(network string, lAddr *net.UDPAddr, rAddr *net.UDPAddr) (*net.UDPConn, error) {
	rSockAddr, err := udpAddrToSockAddr(rAddr)
	if err != nil {
		return nil, err
	}

	lSockAddr, err := udpAddrToSockAddr(lAddr)
	if err != nil {
		return nil, err
	}

	fd, err := syscall.Socket(syscall.AF_INET, syscall.SOCK_DGRAM, 0)
	if err != nil {
		return nil, err
	}

	if err = syscall.SetsockoptInt(fd, syscall.SOL_SOCKET, syscall.SO_REUSEADDR, 1); err != nil {
		syscall.Close(fd)
		return nil, err
	}

	if err = syscall.SetsockoptInt(fd, syscall.SOL_IP, syscall.IP_TRANSPARENT, 1); err != nil {
		syscall.Close(fd)
		return nil, err
	}

	if err = syscall.Bind(fd, lSockAddr); err != nil {
		syscall.Close(fd)
		return nil, err
	}

	if err = syscall.Connect(fd, rSockAddr); err != nil {
		syscall.Close(fd)
		return nil, err
	}

	fdFile := os.NewFile(uintptr(fd), fmt.Sprintf("net-udp-dial-%s", rAddr.String()))
	defer fdFile.Close()

	c, err := net.FileConn(fdFile)
	if err != nil {
		syscall.Close(fd)
		return nil, err
	}

	return c.(*net.UDPConn), nil
}

func stringToUdpAddr(s string) (*net.UDPAddr, error) {
	addr_str, port_str, err := net.SplitHostPort(s)
	if err != nil {
		return nil, err
	}

	ip := net.ParseIP(addr_str)
	port, err := strconv.Atoi(port_str)
	if err != nil {
		return nil, fmt.Errorf("invalid address: %s", s)
	}

	return &net.UDPAddr{IP: ip, Port: port}, nil
}

func udpAddrToSockAddr(addr *net.UDPAddr) (syscall.Sockaddr, error) {
	switch {
	case addr.IP.To4() != nil:
		ip := [4]byte{}
		copy(ip[:], addr.IP.To4())
		return &syscall.SockaddrInet4{Addr: ip, Port: addr.Port}, nil

	default:
		ip := [16]byte{}
		copy(ip[:], addr.IP.To16())
		zoneID, err := strconv.ParseUint(addr.Zone, 10, 32)
		if err != nil {
			zoneID = 0
		}
		return &syscall.SockaddrInet6{Addr: ip, Port: addr.Port, ZoneId: uint32(zoneID)}, nil
	}
}

func makeMarkedDialer(addr string, mark int) (*net.Dialer, error) {
	return &net.Dialer{
		Control: func(_, _ string, c syscall.RawConn) error {
			return c.Control(func(fd uintptr) {
				syscall.SetsockoptInt(int(fd), syscall.SOL_SOCKET, syscall.SO_MARK, mark)
			})
		},
		Resolver: &net.Resolver{
			PreferGo: false,
			Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
				d := net.Dialer{
					Control: func(_, _ string, c syscall.RawConn) error {
						return c.Control(func(fd uintptr) {
							syscall.SetsockoptInt(int(fd), syscall.SOL_SOCKET, syscall.SO_MARK, mark)
						})
					},
				}
				return d.DialContext(ctx, network, address)
			},
		},
	}, nil
}

func websocketDialer(network, addr string) (net.Conn, error) {
	return dialBypassProxy(network, addr)
}

func dialBypassProxy(network, address string) (net.Conn, error) {
	dialer, err := makeMarkedDialer(address, cfg.SocketMarkValue)
	if err != nil {
		return nil, err
	}
	return dialer.Dial(network, address)
}
