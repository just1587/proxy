package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"
)

type serverLatency struct {
	server  string
	latency time.Duration
}

func formatServers(servers []string) ([]string, error) {
	var ret []string
	if len(servers) == 0 {
		return ret, fmt.Errorf("invalid server-list config")
	}
	for _, s := range servers {
		if !strings.HasPrefix(s, "http") {
			s = "http://" + s
		}
		u, err := url.Parse(s)
		if err != nil {
			return ret, err
		}
		//apply default port
		if !regexp.MustCompile(`:\d+$`).MatchString(u.Host) {
			if u.Scheme == "https" || u.Scheme == "wss" {
				u.Host = u.Host + ":443"
			} else {
				u.Host = u.Host + ":80"
			}
		}
		//swap to websockets scheme
		u.Scheme = strings.Replace(u.Scheme, "http", "ws", 1)
		ret = append(ret, u.String())
	}
	return ret, nil
}

func (t *Tunnel) checkServerLatency(sl *serverLatency, wg *sync.WaitGroup) {
	defer wg.Done()
	t0 := time.Now()

	transport := &http.Transport{
		DisableKeepAlives: true,
		TLSClientConfig:   t.tlsConfig.Clone(),
		Dial:              websocketDialer,
	}

	httpClient := &http.Client{
		Transport: transport,
		Timeout:   time.Second * 5,
	}

	url := strings.Replace(sl.server, "ws", "http", 1)
	response, err := httpClient.Get(url)
	if response != nil {
		defer response.Body.Close()
	}
	if err != nil {
		t.Debugf("[TUNNEL] checkServerLatency httpClient.Get failed: %v", err)
		return
	}

	_, err = ioutil.ReadAll(response.Body)
	if err != nil {
		t.Debugf("[TUNNEL] checkServerLatency ioutil.ReadAll failed: %v", err)
		return
	}

	sl.latency = time.Since(t0)
}

func (t *Tunnel) chooseServer(servers []string) string {
	var wg sync.WaitGroup

	if len(servers) == 1 {
		return servers[0]
	}

	latencys := make([]serverLatency, len(servers))
	for i, s := range servers {
		wg.Add(1)
		latencys[i].server = s
		latencys[i].latency = time.Second * 10
		go t.checkServerLatency(&latencys[i], &wg)
	}
	wg.Wait()

	bestServerIdx := 0
	for i, sl := range latencys {
		if sl.latency < latencys[bestServerIdx].latency {
			bestServerIdx = i
		}
	}

	t.Debugf("[TUNNEL] latency: %+v", latencys)
	return latencys[bestServerIdx].server
}
