package main

import (
	"context"
	"fmt"
	"io"
	"net"

	chshare "github.com/jpillora/chisel/share"
	"github.com/jpillora/sizestr"
	"golang.org/x/crypto/ssh"
)

//sshTunnel exposes a subset of Tunnel to subtypes
type sshTunnel interface {
	getSSH(ctx context.Context, wait bool) ssh.Conn
	resolveDomainToFake(domain string) string
	CountStats(statsType int, n int64)
	isBypassProxy(ip string) (string, bool)
	handleDNSrequest(conn *net.UDPConn, addr *net.UDPAddr, remoteAddr string, pkt []byte) error
}

//Proxy is the portion of a Tunnel
type Proxy struct {
	l      *chshare.Logger
	sshTun sshTunnel
	id     int
	count  int
	remote *chshare.Remote
	tcp    *net.TCPListener
	udp    *udpListener
}

func NewProxy(logger *chshare.Logger, sshTun sshTunnel, index int, remote *chshare.Remote) (*Proxy, error) {
	id := index + 1
	p := &Proxy{
		l:      logger.Fork("proxy#%d:%s/%s", id, remote.LocalPort, remote.LocalProto),
		sshTun: sshTun,
		id:     id,
		remote: remote,
	}
	return p, p.listen()
}

func (p *Proxy) listen() error {
	if p.remote.LocalProto == "tcp" {
		addr, err := net.ResolveTCPAddr("tcp", p.remote.LocalHost+":"+p.remote.LocalPort)
		if err != nil {
			return fmt.Errorf("resolve: %s", err)
		}
		l, err := net.ListenTCP("tcp", addr)
		if err != nil {
			return fmt.Errorf("tcp: %s", err)
		}
		err = setTcpListeningSocketOption(l)
		if err != nil {
			return fmt.Errorf("tcp: %s", err)
		}
		p.l.Infof("[PROXY] [TCP] Listening")
		p.tcp = l
	} else if p.remote.LocalProto == "udp" {
		l, err := listenUDP(p.l, p.sshTun, p.remote)
		if err != nil {
			return err
		}
		p.l.Infof("[PROXY] [UDP] Listening")
		p.udp = l
	} else {
		return fmt.Errorf("[PROXY] unknown local proto")
	}
	return nil
}

//Run enables the proxy and blocks while its active,
//close the proxy by cancelling the context.
func (p *Proxy) Run(ctx context.Context) error {
	if p.remote.LocalProto == "tcp" {
		return p.runTCP(ctx)
	} else if p.remote.LocalProto == "udp" {
		return p.udp.run(ctx)
	}
	return fmt.Errorf("invalid local proto")
}

func (p *Proxy) runTCP(ctx context.Context) error {
	done := make(chan struct{})
	go func() {
		select {
		case <-ctx.Done():
			p.tcp.Close()
			p.l.Infof("[PROXY] [TCP] Closed")
		case <-done:
		}
	}()
	for {
		src, err := p.tcp.Accept()
		if err != nil {
			select {
			case <-ctx.Done():
				//listener closed
				err = nil
			default:
				p.l.Infof("[PROXY] [TCP] Accept error: %s", err)
			}
			close(done)
			return err
		}

		p.sshTun.CountStats(StatsTcpConns, 1)
		local := src.RemoteAddr().String()
		remote := p.remote.Remote()
		if !p.remote.Socks {
			remoteHost, remotePort := getOriginalRemoteAddr(src, p.l)
			remote = fmt.Sprintf("%s:%s", remoteHost, remotePort)
			localHost, localPort := getOriginalLocalAddr(src, p.l)
			local = fmt.Sprintf("%s:%s", localHost, localPort)
			remote, ok := p.sshTun.isBypassProxy(remoteHost)
			if ok {
				go p.pipeRealServer(ctx, src, remote)
			} else {
				go p.pipeRemote(ctx, src, local, remote)
			}
		}
		go p.pipeRemote(ctx, src, local, remote)
	}
}

func (p *Proxy) pipeRemote(ctx context.Context, src io.ReadWriteCloser, srcAddr string, remote string) {
	defer func() {
		src.Close()
	}()
	p.count++
	cid := p.count
	l := p.l.Fork("conn#%d", cid)
	l.Debugf("[PROXY] [TCP] Open %s", p.remote.Remote())
	sshConn := p.sshTun.getSSH(ctx, false)
	if sshConn == nil {
		p.sshTun.CountStats(ErrorTcpRemoteProxy, 1)
		l.Debugf("[PROXY] [TCP] No remote connection")
		return
	}

	dst, reqs, err := sshConn.OpenChannel("tunnel", []byte(remote+"/tcp"))
	if err != nil {
		p.sshTun.CountStats(ErrorTcpRemoteProxy, 1)
		l.Infof("[PROXY] [TCP] tunnel OpenChannel error: %s", err)
		return
	}
	go ssh.DiscardRequests(reqs)

	//then pipe
	f1, f2 := p.countTcpTrafficBytes()
	s, r := chshare.Pipe2(src, dst, f1, f2)
	l.Debugf("[PROXY] [TCP] Close %s, sent %s, received %s", p.remote.Remote(), sizestr.ToString(s), sizestr.ToString(r))
}

func (p *Proxy) pipeRealServer(ctx context.Context, src io.ReadWriteCloser, remote string) {
	defer func() {
		src.Close()
	}()

	p.count++
	cid := p.count
	l := p.l.Fork("conn#rs#%d", cid)
	l.Debugf("[BYPASS] [TCP] Open %s", remote)

	dst, err := dialBypassProxy("tcp", remote)
	if err != nil {
		p.sshTun.CountStats(ErrorTcpLocalProxy, 1)
		l.Infof("[BYPASS] [TCP] Dial error: %s", err)
		return
	}
	defer dst.Close()
	s, r := chshare.Pipe(src, dst)
	l.Debugf("[BYPASS] [TCP] Close %s, sent %s, received %s", remote, sizestr.ToString(s), sizestr.ToString(r))
}

func (p *Proxy) countTcpTrafficBytes() (f1, f2 func(int64)) {
	f1 = func(n int64) {
		p.sshTun.CountStats(StatsBytesSent, n)
	}
	f2 = func(n int64) {
		p.sshTun.CountStats(StatsBytesReceived, n)
	}
	return
}
