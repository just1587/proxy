package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	chshare "github.com/jpillora/chisel/share"
)

type appCfg struct {
	FingerPrint         string   `json:"fingerprint"`
	CaFile              string   `json:"ca-file"`
	KeepAlive           int      `json:"keepalive"`
	MaxRetryCount       int      `json:"max-retry-count"`
	MaxRetryInterval    int      `json:"max-retry-interval"`
	SkipTlsVerification bool     `json:"skip-tls-verification"`
	Pid                 bool     `json:"pid"`
	Hostname            string   `json:"hostname"`
	Verbose             bool     `json:"verbose"`
	ServerList          []string `json:"server-list"`
	Remote              string   `json:"local-port"`
	RemoteUDP           string   `json:"local-port-udp"`
	StatsServerPort     string   `json:"stats-server-port"`
	InterfaceName       string   `json:"interface-name"`
	DnsServers          []string `json:"dns-servers"`
	SocketMarkValue     int      `json:"socket-mark-value"`
}

var (
	cfg             appCfg
	thisClient      *Client
	buildTimestamp  string
	buildGitRevison string
)

func validConfig(buf string) error {
	err := json.Unmarshal([]byte(buf), &cfg)
	if err != nil {
		return err
	}

	if cfg.InterfaceName == "" {
		cfg.InterfaceName = "en0"
	}

	if runtime.GOOS == "linux" && cfg.SocketMarkValue == 0 {
		cfg.SocketMarkValue = 0xff
	}

	return nil
}

func main() {
	pConfigFile := flag.String("f", "./conf.json", "config file path")
	pAppVersion := flag.Bool("v", false, "show version")
	flag.Parse()

	if *pAppVersion {
		fmt.Printf("OS: %s-%s\nVersion: %s-%s\nBuild Time: %s\n", runtime.GOOS,
			runtime.GOARCH, chshare.BuildVersion, buildGitRevison, buildTimestamp)
		os.Exit(1)
	}

	buf, err := ioutil.ReadFile(*pConfigFile)
	if err != nil {
		log.Fatalf("read config file %s error: %v\n", *pConfigFile, err)
	}

	err = validConfig(string(buf))
	if err != nil {
		log.Fatalf("valid config file %s error: %v\n", *pConfigFile, err)
	}

	signal.Ignore(syscall.SIGPIPE)

	remotes := []string{cfg.Remote}
	if cfg.RemoteUDP != "" {
		remotes = append(remotes, cfg.RemoteUDP+"/udp")
	}
	thisClient, err = NewClient(&Config{
		Fingerprint:         cfg.FingerPrint,
		CAFile:              cfg.CaFile,
		KeepAlive:           time.Duration(cfg.KeepAlive) * time.Second,
		MaxRetryCount:       cfg.MaxRetryCount,
		MaxRetryInterval:    time.Duration(cfg.MaxRetryInterval) * time.Second,
		ServerList:          cfg.ServerList,
		SkipTlsVerification: cfg.SkipTlsVerification,
		Remotes:             remotes,
		HostHeader:          cfg.Hostname,
		Debug:               cfg.Verbose,
	})
	if err != nil {
		log.Fatal(err)
	}
	if cfg.Pid {
		generatePidFile()
	}
	ctx := chshare.InterruptContext()
	if err := thisClient.Start(ctx); err != nil {
		log.Fatal(err)
	}
	if err := thisClient.Wait(); err != nil {
		log.Fatal(err)
	}
}

func generatePidFile() {
	pid := []byte(strconv.Itoa(os.Getpid()))
	if err := ioutil.WriteFile("chisel.pid", pid, 0644); err != nil {
		log.Fatal(err)
	}
}
