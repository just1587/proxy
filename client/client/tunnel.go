package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	chshare "github.com/jpillora/chisel/share"
	"golang.org/x/crypto/ssh"
	"golang.org/x/sync/errgroup"
)

type tunnelConfig struct {
	*chshare.Logger
	keepAlive time.Duration
}

type Tunnel struct {
	sync.RWMutex
	statsCount [StatsTypeMax]int64
	//config
	tunnelConfig
	tlsConfig *tls.Config
	//ssh connection
	activeConnMut  sync.RWMutex
	activatingConn sync.WaitGroup
	activeConn     ssh.Conn
	activeServer   string
	servers        []string
	bypassDns      []string
	//proxies
	proxyCount int
	dns        *dnsFakeResolver
	//internals
	connStats chshare.ConnStats
}

func NewTunnel(c tunnelConfig) *Tunnel {
	t := &Tunnel{tunnelConfig: c}
	t.activatingConn.Add(1)
	t.StartDNSResolver()
	return t
}

//BindSSH provides an active SSH for use for tunnelling
func (t *Tunnel) BindSSH(ctx context.Context, c ssh.Conn, reqs <-chan *ssh.Request, chans <-chan ssh.NewChannel) error {
	//mark active and unblock
	go func() {
		<-ctx.Done()
		if c.Close() == nil {
			t.Debugf("[TUNNEL] SSH cancelled")
		}
	}()

	t.activeConnMut.Lock()
	if t.activeConn != nil {
		panic("double bind ssh")
	}
	t.activeConn = c
	t.activeConnMut.Unlock()
	t.activatingConn.Done()
	//optional keepalive loop against this connection
	if t.tunnelConfig.keepAlive > 0 {
		go t.keepAliveLoop(c)
	}
	//block until closed
	go ssh.DiscardRequests(reqs)
	go t.discardStreams(chans)
	t.Debugf("[TUNNEL] SSH connected")
	err := c.Wait()
	t.Debugf("[TUNNEL] SSH disconnected")
	//mark inactive and block
	t.activatingConn.Add(1)
	t.activeConnMut.Lock()
	t.activeConn = nil
	t.activeConnMut.Unlock()
	return err
}

//getSSH blocks while connecting
func (t *Tunnel) getSSH(ctx context.Context, wait bool) ssh.Conn {
	//cancelled already?
	if isDone(ctx) {
		return nil
	}
	t.activeConnMut.RLock()
	c := t.activeConn
	t.activeConnMut.RUnlock()
	//connected already?
	if c != nil || !wait {
		return c
	}
	//connecting...
	select {
	case <-ctx.Done(): //cancelled
		return nil
	case <-t.activatingConnWait():
		t.activeConnMut.RLock()
		c := t.activeConn
		t.activeConnMut.RUnlock()
		return c
	}
}

func (t *Tunnel) isConnected() bool {
	t.activeConnMut.RLock()
	c := t.activeConn
	t.activeConnMut.RUnlock()
	if c == nil {
		return false
	}
	return true
}

func (t *Tunnel) CountStats(statsType int, n int64) {
	if statsType >= StatsTypeMax {
		return
	}
	atomic.AddInt64(&t.statsCount[statsType], n)
}

func (t *Tunnel) activatingConnWait() <-chan struct{} {
	ch := make(chan struct{})
	go func() {
		t.activatingConn.Wait()
		close(ch)
	}()
	return ch
}

//BindRemotes converts the given remotes into proxies, and blocks
//until the caller cancels the context or there is a proxy error.
func (t *Tunnel) BindRemotes(ctx context.Context, remotes []*chshare.Remote) error {
	if len(remotes) == 0 {
		return errors.New("no remotes")
	}
	proxies := make([]*Proxy, len(remotes))
	for i, remote := range remotes {
		p, err := NewProxy(t.Logger, t, t.proxyCount, remote)
		if err != nil {
			return err
		}
		proxies[i] = p
		t.proxyCount++
	}
	//TODO: handle tunnel close
	eg, ctx := errgroup.WithContext(ctx)
	for _, proxy := range proxies {
		p := proxy
		eg.Go(func() error {
			return p.Run(ctx)
		})
	}
	t.Debugf("[TUNNEL] Bound proxies")
	err := eg.Wait()
	t.Debugf("[TUNNEL] Unbound proxies")
	return err
}

func (t *Tunnel) keepAliveLoop(sshConn ssh.Conn) {
	//ping forever
	for {
		time.Sleep(t.tunnelConfig.keepAlive)
		c := make(chan bool, 1)
		go func() {
			_, b, err := sshConn.SendRequest("ping", true, nil)
			if err != nil {
				t.Debugf("[TUNNEL] send ping request failed: %v", err)
				c <- false
				return
			}
			if len(b) > 0 && !bytes.Equal(b, []byte("pong")) {
				t.Debugf("[TUNNEL] strange ping response")
				c <- false
				return
			}
			c <- true
		}()

		var ret bool
		select {
		case ret = <-c:
			break
		case <-time.After(t.tunnelConfig.keepAlive):
			t.Debugf("[TUNNEL] keepalive ping timeout")
			break
		}
		if ret == false {
			break
		}
	}
	//close ssh connection on abnormal ping
	sshConn.Close()
}

func (t *Tunnel) discardStreams(chans <-chan ssh.NewChannel) {
	for ch := range chans {
		stream, reqs, err := ch.Accept()
		if err != nil {
			t.Debugf("[TUNNEL] Failed to accept stream: %s", err)
			continue
		}
		go ssh.DiscardRequests(reqs)
		stream.Close()
	}
}

func (t *Tunnel) getTunnelStatus() string {
	var ret tunnelStats
	ret.TunnelConnected = t.isConnected()
	if ret.TunnelConnected {
		ret.TunnelServer = strings.Replace(t.activeServer, "ws", "http", 1)
	}
	retBytes, _ := json.MarshalIndent(&ret, "", "\t")
	return string(retBytes)
}

func (t *Tunnel) StartDNSResolver() {
	t.dns = NewDnsFakeResolver(&dnsFakeResolverConfig{
		Logger:  t.Logger,
		startIp: 0xc6120000, //198.18.0.0/16
		ipCount: 65536,
	})
}
